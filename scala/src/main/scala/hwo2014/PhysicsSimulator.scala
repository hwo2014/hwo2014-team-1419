package hwo2014

import hwo2014.Messages.TurboAvailable

class PhysicsSimulator(ticksPerSecond: Double, friction: Double, acceleration: Double) {

  def shouldUseTurbo(desiredVelocity: Double, distanceAway: Double, currentVelocity: Double, turbo: Option[TurboAvailable]): Boolean = {
    if (turbo.isDefined) {
      val accelerationPerTick = turbo.get.turboFactor * acceleration / ticksPerSecond
      var velocityPerTick = currentVelocity / ticksPerSecond

      var distanceTravelled = 0.0
      for (i <- 0 until turbo.get.turboDurationTicks) {
        val previousVelocityPerTick = velocityPerTick
        velocityPerTick = calculateNextVelocity(velocityPerTick, friction, accelerationPerTick)
        val distance = velocityPerTick - previousVelocityPerTick
        distanceTravelled += distance
      }

      val exitTickVelocity = velocityPerTick
      val exitVelocity = exitTickVelocity * ticksPerSecond
      val distanceRemaining = distanceAway - distanceTravelled
      val ticksTilTarget = (distanceRemaining / exitTickVelocity).floor
      val velocityAtTarget = Math.pow(friction, ticksTilTarget) * exitVelocity

      println(s"shouldUseTurbo? ticksTilTarget=$ticksTilTarget distanceRemaining=$distanceRemaining exitVelocity=$exitVelocity desiredVelocity=$desiredVelocity velocityAtTarget=$velocityAtTarget")
      if (ticksTilTarget >= 5) {
        if (velocityAtTarget <= desiredVelocity) {
          println(s"YES use turbo!")
          true
        } else {
          println(s"too close for turbo")
          false
        }
      } else {
        println(s"ticks too close for turbo")
        false
      }
    } else {
      false
    }
  }

  def throttleForDistantDesiredVelocity(desiredVelocity: Double, distanceAway: Double, currentVelocity: Double, activeTurbo: Option[TurboAvailable]): Double = {
    val accel = if (activeTurbo.isDefined) acceleration * activeTurbo.get.turboFactor else acceleration
    throttleForDistantDesiredVelocity(desiredVelocity, distanceAway, currentVelocity, accel)
  }

  def throttleForDistantDesiredVelocity(desiredVelocity: Double, distanceAway: Double, currentVelocity: Double, acceleration: Double): Double = {
    var futureVelocity = currentVelocity
    futureVelocity = calculateNextVelocity(futureVelocity, friction, acceleration)
    futureVelocity = calculateNextVelocity(futureVelocity, friction, acceleration)

    if (futureVelocity <= desiredVelocity) {
      println("speeding up to desired velocity")
      1.0
    } else {
      val effectiveTickVelocity = futureVelocity / ticksPerSecond
      val ticksTilTarget = (distanceAway / effectiveTickVelocity).floor
      val velocityAtTarget = Math.pow(friction, ticksTilTarget) * currentVelocity
      println(s"distanceAway: $distanceAway ticksAway: $ticksTilTarget velocity: $velocityAtTarget")
      if (ticksTilTarget >= 5) {
        if (velocityAtTarget > desiredVelocity) {
          println("rate-limiting and maintaining speed")
          0.0
        } else {
          println("lets go faster!")
          1.0
        }
      } else {
        println("too close to target, maintaining speed")
        0.0
      }
    }
  }

  def throttleForDesiredVelocity(desiredVelocity: Double, currentVelocity: Double): Double = {
    val futureVelocity1 = calculateNextVelocity(currentVelocity, friction, acceleration)
    val futureVelocity2 = calculateNextVelocity(futureVelocity1, friction, acceleration)

    if (futureVelocity2 <= desiredVelocity) {
      1.0
    } else if (futureVelocity1 <= desiredVelocity) {
      0.1
    } else {
      0.0
    }
  }

  private[this] def calculateNextVelocity(velocity: Double, friction: Double, acceleration: Double) =
    (velocity + acceleration) * friction
}
