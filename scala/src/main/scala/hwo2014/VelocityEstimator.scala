package hwo2014

import java.io.{PrintWriter, File}
import org.encog.neural.networks.BasicNetwork
import org.encog.persist.EncogDirectoryPersistence
import org.encog.neural.networks.layers.BasicLayer
import org.encog.engine.network.activation.{ActivationLinear, ActivationSigmoid}
import org.encog.ml.data.basic.{BasicMLData, BasicMLDataSet}
import org.encog.ml.data.MLData
import org.encog.ml.train.strategy.RequiredImprovementStrategy
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation

object VelocityEstimator {
  case class Input(angle: Double, radius: Double, carAngle: Double)
  case class TrainingData(input: Input, output: Double)

  private val MAX_VELOCITY = 800.0
}

class VelocityEstimator {
  import VelocityEstimator._

  // create a neural network, without using a factory
  val file = new File("velocityEstimator.eg")
  lazy val trainingFile = new File(System.nanoTime() + ".training")
  lazy val writer = new PrintWriter(trainingFile)
  val network: BasicNetwork = if (file.exists) {
    EncogDirectoryPersistence.loadObject(file).asInstanceOf[BasicNetwork]
  } else {
    val network = new BasicNetwork()
    val numInputs = 2
    network.addLayer(new BasicLayer(null, true, numInputs))
    network.addLayer(new BasicLayer(new ActivationLinear(), false, 1))
    network.getStructure.finalizeStructure()
    network.reset()
    network
  }

  var count = 0
  def train(data: TrainingData, logData: Boolean) {
    val input = data.input
    val output = data.output
    if (logData) {
      writer.write(s"$data\n")
      writer.flush()
    }
    println("training iteration: " + count)
    count += 1
    val inputSet = Array(inputToArray(input))
    val outputSet = Array(Array(output / MAX_VELOCITY))
    trainNetwork(inputSet, outputSet)
  }

  def train(data: Seq[TrainingData]) {
    val (inputs, outputs) = data.unzip { case TrainingData(input, output) => (inputToArray(input), Array(output / MAX_VELOCITY)) }
    trainNetwork(inputs.toArray, outputs.toArray)
  }

  def save() {
    EncogDirectoryPersistence.saveObject(file, network)
  }

  def computeMaxVelocity(data: Input): Double = {
    val output = network.compute(inputToEncog(data)).getData(0)
    output * MAX_VELOCITY
  }

  private[this] def trainNetwork(input: Array[Array[Double]], output: Array[Array[Double]]) {
    val trainingSet = new BasicMLDataSet(input, output)
    import scala.collection.JavaConversions._
    trainingSet.getData.foreach { data =>
      println(s"input: ${data.getInput.getData.toSeq}")
      println(s"output: ${data.getIdeal.getData.toSeq}")
    }

    // train the neural network
    val train = new ResilientPropagation(network, trainingSet)
    // reset if improve is less than 1% over 5 cycles
    train.addStrategy(new RequiredImprovementStrategy(5))

    var epoch = 1

    do {
      train.iteration()
      println("Epoch #" + epoch + " Error:" + train.getError()*100)
      println("Estimated output: " + network.compute(trainingSet.get(0).getInput).getData(0))
      epoch += 1
    } while (train.getError() > 0.0006 || epoch < 200)
  }

  private[this] def inputToArray(input: Input): Array[Double] = {
    Array(input.angle.abs / 360.0, input.radius.abs / 500.0)//, input.carAngle.abs / 70.0)
  }

  private[this] def inputToEncog(input: Input): MLData = {
    new BasicMLData(inputToArray(input))
  }
}
