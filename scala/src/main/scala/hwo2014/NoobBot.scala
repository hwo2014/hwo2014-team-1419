package hwo2014

import org.json4s._
import java.net.Socket
import java.io._
import org.json4s.native.Serialization
import scala.annotation.tailrec
import scala.collection.mutable
import hwo2014.Messages._
import scala.collection.mutable.ArrayBuffer
import scala.util.Random

case class Bend(radius: Double, angle: Double, distanceAway: Double, pieceIndex: Int)

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: velocityOverride :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey, Some(velocityOverride.toDouble))
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey, None)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String, velocityOverride: Option[Double]) {
  implicit val formats = new DefaultFormats {}

  val velocityEstimator = new VelocityEstimator
  if (!velocityOverride.isDefined) {
    velocityEstimator.train(TrainingDataYay.cleanedData)
    TrainingDataYay.cleanedData.foreach { x =>
      println(s"Estimating input: ${x.input} ${velocityEstimator.computeMaxVelocity(x.input)}")
    }
    //val testData = VelocityEstimator.TrainingData(VelocityEstimator.Input(45.0,100.0,0.0), 356.4419543322765)
    //velocityEstimator.train(testData, logData=false)
    //velocityEstimator.computeMaxVelocity(testData.input)
  }

  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))

  val ticksPerSecond = 60
  val friction = 0.98
  // upon submitting 0 throttle, one round of nop then it takes effect
  val acceleration = 11.980859391122081
  val physicsSim = new PhysicsSimulator(ticksPerSecond=ticksPerSecond, friction=friction, acceleration=acceleration)

  var gameInitData: GameInitData = null
  var pieces: Seq[Piece] = null
  var bendPieces: Seq[Piece] = null
  var pieceDistances: ArrayBuffer[ArrayBuffer[Double]] = null

  case class CarState(var distanceTravelled: Double, var velocity: Double, var angle: Double, var angularVelocity: Double, var pieceIndex: Int, var endOfPieceDistance: Double, var crashed: Boolean, var needsInit: Boolean, var velocityErrorCount: Int)
  val carStates: mutable.Map[String, CarState] = mutable.Map[String, CarState]()
  var trainingDataList: List[VelocityEstimator.TrainingData] = Nil
  var myColor: String = null

  val random = new Random()
  var throttle = 1.0
  var turbo: Option[TurboAvailable] = None
  var lastTurbo: Option[TurboAvailable] = None
  var turboActive: Boolean = false

  send(MsgWrapper("join", Join(botName, botKey), None))
  //val newRace = NewRace(Join(botName, botKey), "germany", carCount=1)
  //send(MsgWrapper("createRace", newRace, None))
  //send(MsgWrapper("joinRace", newRace, None))
  play()

  def getCurrentBend(carPosition: CarPosition): Option[Bend] = {
    val laneIndex = carPosition.piecePosition.lane.endLaneIndex
    var bendPiece: Piece = null
    val i = carPosition.piecePosition.pieceIndex
    val piece = pieces(i)
    if (piece.angle.isDefined) {
      bendPiece = piece
    }
    if (bendPiece == null) {
      None
    } else {
      Some(Bend(bendPiece.radius.get - gameInitData.race.track.lanes(laneIndex).distanceFromCenter, bendPiece.angle.get, 0, i))
    }
  }

  def findNextBend(carPosition: CarPosition): Bend = {
    val laneIndex = carPosition.piecePosition.lane.endLaneIndex
    var bendPiece: Piece = null
    var i = carPosition.piecePosition.pieceIndex
    var distance = pieceDistances(i)(laneIndex) - carPosition.piecePosition.inPieceDistance
    while (bendPiece == null) {
      i = (i + 1) % pieceDistances.length
      val piece = pieces(i)
      val pieceLength = pieceDistances(i)(laneIndex)
      if (piece.angle.isDefined) {
        bendPiece = piece
      } else {
        distance += pieceLength
      }
    }
    Bend(bendPiece.radius.get - gameInitData.race.track.lanes(laneIndex).distanceFromCenter, bendPiece.angle.get, distance, i)
  }

  var throttleCount = 0

  def computeMaxVelocity(bend: Bend, carPosition: CarPosition): Double = {
    velocityEstimator.computeMaxVelocity(VelocityEstimator.Input(bend.angle.abs, bend.radius, 0.0))//carPosition.angle))
  }

  private[this] def initCar(carState: CarState, carPosition: CarPosition) {
    val piecePosition = carPosition.piecePosition
    carState.pieceIndex = piecePosition.pieceIndex
    carState.distanceTravelled = piecePosition.inPieceDistance
    carState.velocity = 0.0
    carState.angle = carPosition.angle
    carState.angularVelocity = 0.0
    carState.velocityErrorCount = 0
    carState.endOfPieceDistance = pieceDistances(piecePosition.pieceIndex)(piecePosition.lane.endLaneIndex)
    carState.needsInit = false
  }

  @tailrec
  private def play() {
    var useTurbo = false
    val line = reader.readLine()
    if (line != null) {
      Serialization.read[MsgWrapper](line) match {
        case MsgWrapper("carPositions", data, gameTick) =>
          val carPositions = data.extract[List[CarPosition]]
          carPositions.foreach { carPosition =>
            val piecePosition = carPosition.piecePosition
            val carState = carStates(carPosition.id.color)

            if (carState.needsInit && !carState.crashed) {
              initCar(carState, carPosition)
            } else {
              val prevDistance = carState.distanceTravelled
              val prevVelocity = carState.velocity
              val prevPieceIndex = carState.pieceIndex
              val currentPieceDistance = pieceDistances(piecePosition.pieceIndex)(piecePosition.lane.endLaneIndex)
              val currentBend: Option[Bend] = getCurrentBend(carPosition)
              val calculatedDistance = if (prevPieceIndex != piecePosition.pieceIndex) {
                // moved to a new piece, so add on the old piece full distance
                val oldEndOfPieceDistance = carState.endOfPieceDistance
                val newDistance = oldEndOfPieceDistance + piecePosition.inPieceDistance
                val newEndOfPieceDistance = oldEndOfPieceDistance + currentPieceDistance
                carState.endOfPieceDistance = newEndOfPieceDistance
                //println(s"changed piece newDistance=$newDistance")
                newDistance
              } else {
                val newDistance = carState.endOfPieceDistance + piecePosition.inPieceDistance - currentPieceDistance
                //println(s"same piece newDistance=$newDistance")
                newDistance
              }

              if (calculatedDistance < prevDistance) {
                println(s"distance=$calculatedDistance prev=$prevDistance ipd=${piecePosition.inPieceDistance}")
              }

              // fix it up if the calculation was iffy
              val distance = if (calculatedDistance < prevDistance) prevDistance else calculatedDistance
              //println(s"calculatedDistance=$calculatedDistance prevDistance=$prevDistance")

              val newVelocity = (distance - prevDistance) * ticksPerSecond.toDouble
              val velocityChange = ((newVelocity - prevVelocity) / newVelocity).abs
              val velocity = if (prevVelocity > 72 && velocityChange > 0.2) {
                println("new velocity too different, correct it just by using previous velocity")
                carState.velocityErrorCount += 1
                prevVelocity
              } else {
                newVelocity
              }

              // training
              if (!carState.crashed && prevPieceIndex != piecePosition.pieceIndex && !trainingDataList.isEmpty) {
                // not crashed and moved to a new piece that's not a bend, so we successfully completed a bend
                if (bendPieces(carPosition.piecePosition.pieceIndex) == null) {
                  if (velocityOverride.isDefined) {
                    trainingDataList.foreach { data =>
                      velocityEstimator.train(data, logData = true)
                    }
                  }
                  trainingDataList = Nil
                }
              }

              // if new piece is a bend, update the entry details
              if (prevPieceIndex != piecePosition.pieceIndex && prevPieceIndex >= 0 && currentBend.isDefined) {
                trainingDataList :+= VelocityEstimator.TrainingData(
                  VelocityEstimator.Input(currentBend.get.angle, currentBend.get.radius, carPosition.angle),
                  velocity
                )
              }

              carState.distanceTravelled = distance
              carState.velocity = velocity
              carState.angularVelocity = carPosition.angle.abs - carState.angle.abs
              carState.angle = carPosition.angle
              carState.pieceIndex = piecePosition.pieceIndex

              // something went wrong, restart the car :)
              if (carState.velocityErrorCount >= 2) {
                initCar(carState, carPosition)
              }
            }
          }

          if (velocityOverride.isDefined) {
            val carState = carStates(myColor)
            val carPosition = carPositions.filter(_.id.color == myColor).head
            println(carPosition)
            println(s"velocity=${carState.velocity}")
            throttle = physicsSim.throttleForDesiredVelocity(velocityOverride.get, carState.velocity)
          } else {
            val carState = carStates(myColor)
            val carPosition = carPositions.filter(_.id.color == myColor).head
            println(carPosition)
            println(s"velocity=${carState.velocity}")
            val currentBend: Option[Bend] = getCurrentBend(carPosition)
            val nextBend: Bend = findNextBend(carPosition)
            val nextBendMaxVelocity = computeMaxVelocity(nextBend, carPosition)
            val maxVelocity = currentBend match {
              case Some(bend) =>
                Math.min(nextBendMaxVelocity, computeMaxVelocity(bend, carPosition))
              case None =>
                nextBendMaxVelocity
            }
            val distanceTilDesiredVelocity = currentBend match {
              case Some(bend) =>
                0
              case None =>
                nextBend.distanceAway
            }
            useTurbo = physicsSim.shouldUseTurbo(maxVelocity, distanceTilDesiredVelocity, carState.velocity, turbo)
            val activeTurbo: Option[TurboAvailable] = if (turboActive) lastTurbo else None
            throttle = physicsSim.throttleForDistantDesiredVelocity(maxVelocity, distanceTilDesiredVelocity, carState.velocity, activeTurbo)
            if (currentBend.isDefined && (carState.angularVelocity > 2 || carState.angle.abs > 50)) {
              println(s"throttle override to 0.0 -> car angle too high: ${carPosition.angle} av=${carState.angularVelocity}")
              throttle = 0.0
            }
            println(s"new throttle: $throttle desiredVelocity: $maxVelocity")
          }

        case MsgWrapper("gameInit", data, _) =>
          gameInitData = data.extract[GameInitData]

          val lanes = gameInitData.race.track.lanes
          pieces = gameInitData.race.track.pieces.toIndexedSeq
          bendPieces = pieces.map(x => if (x.angle.isDefined) x else null)
          gameInitData.race.cars.foreach { car =>
            carStates(car.id.color) = CarState(
              distanceTravelled=0.0,
              velocity=0.0,
              angle=0.0,
              angularVelocity=0.0,
              pieceIndex= -1,
              endOfPieceDistance=0.0,
              crashed=false,
              needsInit=true,
              velocityErrorCount=0)
          }

          pieceDistances = new ArrayBuffer[ArrayBuffer[Double]](pieces.length)
          pieces.foreach { piece =>
            var arrayBuffer = new ArrayBuffer[Double](lanes.length)
            if (piece.length.isDefined) {
              lanes.zipWithIndex.foreach { case (lane, i) =>
                arrayBuffer += piece.length.get
              }
            } else {
              lanes.zipWithIndex.foreach { case (lane, i) =>
                val length: Double = (piece.angle.get.abs / 360.0) * 2.0 * Math.PI * (piece.radius.get - lane.distanceFromCenter)
                arrayBuffer += length
              }
            }
            pieceDistances += arrayBuffer
          }

          println(s"${pieces.length} pieces")
          println("gameInit: " + gameInitData)
          pieces.zipWithIndex.foreach(println)
          //pieceDistances.zipWithIndex.foreach(println)

        case MsgWrapper("yourCar", data, gameTick) =>
          val yourCar = data.extract[CarId]
          myColor = yourCar.color

        case MsgWrapper("crash", data, gameTick) =>
          println("\nCRASHED\n")
          val crash = data.extract[CarId]
          val carState = carStates(crash.color)
          carState.crashed = true
          carState.velocity = 0.0
          carState.needsInit = true
          trainingDataList = Nil

        case MsgWrapper("spawn", data, gameTick) =>
          println("\nSPAWNED\n")
          val spawn = data.extract[CarId]
          carStates(spawn.color).crashed = false

        case MsgWrapper("turboAvailable", data, _) =>
          turbo = Some(data.extract[TurboAvailable])
          lastTurbo = turbo

        case MsgWrapper("turboStart", data, _) =>
          if (data.extract[CarId].color == myColor) {
            turboActive = true
          }

        case MsgWrapper("turboEnd", data, _) =>
          if (data.extract[CarId].color == myColor) {
            turboActive = false
          }

        case MsgWrapper(msgType, data, _) =>
          println("Received: " + msgType)
          println("Data: " + data)
      }

      if (useTurbo) {
        turbo = None
        send(MsgWrapper("turbo", "warp speed!", None))
      } else {
        send(MsgWrapper("throttle", throttle, None))
      }
      play()
    }
  }

  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush()
  }
}
