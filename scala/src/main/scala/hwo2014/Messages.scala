package hwo2014

import org.json4s._

object Messages {

  case class MsgWrapper(msgType: String, data: JValue, gameTick: Option[Int])

  object MsgWrapper {
    implicit val formats = new DefaultFormats {}

    def apply(msgType: String, data: Any, gameTick: Option[Int]): MsgWrapper = {
      MsgWrapper(msgType, Extraction.decompose(data), gameTick)
    }
  }

  /** common types */
  case class CarId(name: String, color: String) {
    override def toString: String = { s"'$name'=$color" }
  }

  /** join message */
  case class Join(name: String, key: String)

  case class NewRace(botId: Join, trackName: String, carCount: Int)

  /** carPositions message */
  case class LanePosition(startLaneIndex: Int, endLaneIndex: Int)
  case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: LanePosition, lap: Int)
  case class CarPosition(id: CarId, angle: Double, piecePosition: PiecePosition) {
    override def toString: String = { s"$id angle=$angle $piecePosition" }
  }

  /** gameInit message */
  case class Piece(length: Option[Double], switch: Option[String], radius: Option[Int], angle: Option[Double])
  case class Position(x: Double, y: Double)
  case class StartingPoint(position: Position, angle: Double)
  case class Lane(distanceFromCenter: Double, index: Int)
  case class CarDimensions(length: Double, width: Double, guideFlagPosition: Double)
  case class CarDef(id: CarId, dimensions: CarDimensions)
  case class Track(id: String, name: String, pieces: List[Piece], lanes: List[Lane], startingPoint: StartingPoint)
  case class RaceSession(laps: Option[Int], durationMs: Option[Int], maxLapTimeMs: Option[Int], quickRace: Option[Boolean])
  case class Race(track: Track, cars: List[CarDef], raceSession: RaceSession)
  case class GameInitData(race: Race)

  /** turboAvailable message */
  case class TurboAvailable(turboDurationMilliseconds: Double, turboDurationTicks: Int, turboFactor: Double)
}
